#!/usr/bin/env iocsh.bash
# -----------------------------------------------------------------------------
# EPICS siteMods
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
# EPICS siteApps
# -----------------------------------------------------------------------------
require(rshmc8042) 

# -----------------------------------------------------------------------------
# Utgard-Lab - enrivonment params
# -----------------------------------------------------------------------------
epicsEnvSet(IPADDR,   "172.30.244.139")
epicsEnvSet(IPPORT,   "5025")
epicsEnvSet(PREFIX,   "UTG-RS-HMC8042-001")
epicsEnvSet(PROTOCOL, "rshmc8042.proto")
epicsEnvSet(DEV, "utg-rs-hmc8042-001")
epicsEnvSet(HOST, "172.30.244.139")

iocshLoad("$(rshmc8042_DIR)rshmc8042.iocsh")

iocshLoad("$(E3_COMMON_DIR)/e3-common.iocsh")
